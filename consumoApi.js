//seleccionar todos los elementos que vamos a ocupar
const previous = document.querySelector('#previous');
const next = document.querySelector('#next');
const pokemonContainer = document.querySelector('.pokemon-container');
const spinner = document.querySelector("#spinner");

let offset = 1;
let limit = 8;

previous.addEventListener('click', (event) => {
    if(offset != 1){
        offset -=9;
        removeChildNodes(pokemonContainer);
    mostrarData(offset, limit);
    }
    
});
next.addEventListener('click', () => {
    offset += 9;
    mostrarData(offset, limit);
})

//const url = 'https://db.ygoprodeck.com/api/v7/cardinfo.php?'
function traerPokemon(id){
const url = `https://pokeapi.co/api/v2/pokemon/${id}/`
fetch(url)
.then(response => response.json())
.then(data => crearPokemon(data))
spinner.style.display = "none";

}

function mostrarData(offset, limit) {
    spinner.style.display = "block";
    for (let i = offset; i <= offset + limit; i++) {
        traerPokemon(i)
    }
}
function crearPokemon(pokemon){
    const card = document.createElement('div');
    card.classList.add('pokemon-block');

    //contenedor de la imagen
    const spriteContainer = document.createElement('div');
    spriteContainer.classList.add('img-container');

    const sprite = document.createElement('img');
    sprite.src = pokemon.sprites.front_default;


    spriteContainer.appendChild(sprite);

    const number = document.createElement('p');
    number.textContent = `#${pokemon.id.toString().padStart(3, 0)}`;
    
    const name = document.createElement('p');
    name.classList.add('name');
    name.textContent = pokemon.name;

    card.appendChild(spriteContainer);
    card.appendChild(number);
    card.appendChild(name);

    pokemonContainer.appendChild(card);

}

//Removemos los elementos que ya están dentro de otro elemento
function removeChildNodes(parent){
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}
mostrarData(offset, limit);

// function valores(params) {
//     const container = document.getElementById('container')
//     params.forEach(elements => {
//         container.innerHTML = `
//         ${container.innerHTML}
//         <div class = "card">
//         <img src="https://storage.googleapis.com/ygoprodeck.com/pics/34541863.jpg"

//         `
//     });
// }
